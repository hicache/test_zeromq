cmake_minimum_required(VERSION 3.21.0 FATAL_ERROR)

project(test_zeromq LANGUAGES C CXX)

include_directories(${CMAKE_BINARY_DIR})

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(ZMQ_XSUB_URL "tcp://127.0.0.1:12138")
set(ZMQ_XPUB_URL "tcp://127.0.0.1:12139")
set(ZMQ_CTRL_URL "tcp://127.0.0.1:12140")

if(MSVC)
    add_compile_options(/utf-8)
endif()

configure_file(${CMAKE_CURRENT_SOURCE_DIR}/config.h.in ${CMAKE_BINARY_DIR}/config.h @ONLY)

add_subdirectory(thirdparty)

add_executable(local worker.cpp)
target_link_libraries(local PRIVATE cppzmq-static)
target_compile_definitions(local PRIVATE LOCAL_TOPIC="GIRL" REMOTE_TOPIC="BOY")

add_executable(remote worker.cpp)
target_link_libraries(remote PRIVATE cppzmq-static)
target_compile_definitions(remote PRIVATE LOCAL_TOPIC="BOY" REMOTE_TOPIC="GIRL")

add_executable(proxy proxy.cpp)
target_link_libraries(proxy PRIVATE cppzmq-static)
