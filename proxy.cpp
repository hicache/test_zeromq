#include "config.h"
#include "util.h"
#include "zmq.hpp"
#include <atomic>
#include <chrono>
#include <condition_variable>
#include <format>
#include <functional>
#include <iostream>
#include <mutex>
#include <optional>
#include <thread>
#include <unordered_map>

void controlRoutine(const std::stop_token& stoken, SocketMonitor* monitor, const std::tuple<std::string, int>& statistics_param)
{
    using namespace std::chrono_literals;
    std::unique_ptr<zmq::socket_t> statistics_sock;
    std::mutex mutex;

    while (!stoken.stop_requested()) {
        if (monitor) {
            monitor->check_event();
        }

        // Wait some time
        std::unique_lock<std::mutex> lock(mutex);
        std::condition_variable_any().wait_for(lock, stoken, 10ms, [] { return false; });
    }
}

int main(int argc, char* argv[])
{
    zmq::context_t ctx(1);

    auto xpub = zmq::socket_t(ctx, zmq::socket_type::xpub);
    xpub.bind(ZMQ_XPUB_URL);
    // 设置WELCOME消息，当SUB连接成功时，XPUB将向SUB发送消息“W”
    // xpub.set(zmq::sockopt::xpub_welcome_msg, "W");

    auto xsub = zmq::socket_t(ctx, zmq::socket_type::xsub);
    xsub.bind(ZMQ_XSUB_URL);

    auto ctrl = zmq::socket_t(ctx, zmq::socket_type::rep);
    ctrl.bind(ZMQ_CTRL_URL);

    std::atomic<std::optional<int>> client_count;

    SocketMonitor monitor;
    monitor.init(xpub, "inproc://monitor_" + std::to_string(reinterpret_cast<uint64_t>(&xpub)));
    monitor.watchEvent(SocketMonitor::EventType::Accepted, [&client_count] {
        if (!client_count.load(std::memory_order_acquire).has_value()) {
            client_count.store(0, std::memory_order_release);
        }
        int count = client_count.load(std::memory_order_acquire).value();
        client_count.exchange(++count);
        std::cout << std::format("accept, client count: {}\n", client_count.load(std::memory_order_acquire).value());
    });
    monitor.watchEvent(SocketMonitor::EventType::Disconnected, [&client_count, &ctx] {
        if (!client_count.load(std::memory_order_acquire).has_value()) {
            return;
        }
        int count = client_count.load(std::memory_order_acquire).value();
        --count;
        std::cout << std::format("disconnect, client count: {}\n", count);
        if (count == 0) {
            try {
                zmq::socket_t ctrl_req(ctx, zmq::socket_type::req);
                ctrl_req.set(zmq::sockopt::linger, 10);
                ctrl_req.connect(ZMQ_CTRL_URL);
                ctrl_req.send(zmq::str_buffer("TERMINATE"));
                ctrl_req.close();
            } catch (const zmq::error_t& except) {
                std::cout << std::format("Failed to request TERMINATE: {}\n", except.what());
            }
            return;
        }
        client_count.exchange(count);
    });

    std::jthread ctrl_thd(controlRoutine, &monitor, std::make_tuple<std::string, int>("", 1));

    zmq::proxy_steerable(xsub, xpub, zmq::socket_ref(nullptr), ctrl);

    ctrl_thd.request_stop();
    ctrl_thd.join();

    return 0;
}