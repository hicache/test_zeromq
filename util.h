#pragma once

#include "zmq.hpp"

#include <chrono>
#include <condition_variable>
#include <functional>
#include <iostream>
#include <unordered_map>

class ElapsedTimer {
public:
    void start()
    {
        st_ = std::chrono::steady_clock::now();
    }

    void stop()
    {
        st_ = std::nullopt;
    }

    void restart()
    {
        start();
    }

    [[nodiscard]] bool isValid() const
    {
        return st_.has_value();
    }

    [[nodiscard]] bool hasExpired(int ms) const
    {
        if (!isValid()) {
            return false;
        }
        return *st_ + std::chrono::milliseconds(ms) < std::chrono::steady_clock::now();
    }

private:
    std::optional<std::chrono::steady_clock::time_point> st_;
};

class SocketMonitor : public zmq::monitor_t {
public:
    enum EventType : uint16_t {
        None,
        Connected,
        Accepted,
        Disconnected,
    };

    size_t watchEvent(EventType event_type, const std::function<void()>& event_callback)
    {
        event_callbacks_[event_type].emplace_back(event_callback);
        return event_callbacks_[event_type].size() - 1;
    }

private:
    void dispatchEvent(EventType type) const
    {
        if (event_callbacks_.contains(type)) {
            for (const auto& cb : event_callbacks_.at(type)) {
                cb();
            }
        }
    }

    void on_event_connected(const zmq_event_t& event, const char* addr) override
    {
        std::cout << std::format("connect to '{}'\n", addr);
        dispatchEvent(EventType::Connected);
    }

    void on_event_disconnected(const zmq_event_t& event, const char* addr) override
    {
        std::cout << std::format("disconnected from '{}'\n", addr);
        dispatchEvent(EventType::Disconnected);
    }

    void on_event_accepted(const zmq_event_t& event, const char* addr) override
    {
        std::cout << std::format("accecpt from '{}'\n", addr);
        dispatchEvent(EventType::Accepted);
    }

    void on_monitor_started() override
    {
        std::cout << std::format("on_monitor_started\n");
    }

    void on_event_connect_delayed(const zmq_event_t& event_,
        const char* addr) override
    {
        std::cout << std::format("on_event_connect_delayed. addr '{}'\n", addr);
    }
    void on_event_connect_retried(const zmq_event_t& event_,
        const char* addr) override
    {
        std::cout << std::format("on_event_connect_retried. addr '{}'\n", addr);
    }
    void on_event_listening(const zmq_event_t& event_, const char* addr) override
    {
        std::cout << std::format("on_event_listening. addr '{}'\n", addr);
    }
    void on_event_bind_failed(const zmq_event_t& event_, const char* addr) override
    {
        std::cout << std::format("on_event_bind_failed. addr '{}'\n", addr);
    }

    void on_event_accept_failed(const zmq_event_t& event_, const char* addr) override
    {
        std::cout << std::format("on_event_accept_failed. addr '{}'\n", addr);
    }
    void on_event_closed(const zmq_event_t& event_, const char* addr) override
    {
        std::cout << std::format("on_event_closed. addr '{}'\n", addr);
    }
    void on_event_close_failed(const zmq_event_t& event_, const char* addr) override
    {
        std::cout << std::format("on_event_close_failed. addr '{}'\n", addr);
    }

private:
    std::unordered_map<EventType, std::vector<std::function<void()>>> event_callbacks_;
};