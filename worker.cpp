#include "config.h"
#include "util.h"
#include "zmq.hpp"
#include <chrono>
#include <condition_variable>
#include <format>
#include <functional>
#include <iostream>
#include <mutex>
#include <optional>
#include <print>
#include <thread>
#include <unordered_map>

void subscriber(std::stop_source& stop_source, zmq::context_t& ctx)
{
    using namespace std::chrono_literals;
    try {
        zmq::socket_t xsub(ctx, zmq::socket_type::sub);
        xsub.set(zmq::sockopt::subscribe, zmq::str_buffer(REMOTE_TOPIC));
        xsub.connect(ZMQ_XPUB_URL);

        SocketMonitor monitor;
        monitor.init(xsub, "inproc://monitor_" + std::to_string(reinterpret_cast<uint64_t>(&xsub)));

        monitor.watchEvent(SocketMonitor::EventType::Disconnected, [&stop_source] {
            stop_source.request_stop();
        });

        const auto& stoken = stop_source.get_token();
        ElapsedTimer timer;
        std::mutex mutex;

        timer.start();

        while (!stoken.stop_requested()) {
            monitor.check_event();

            zmq::message_t msg;
            auto result_opt = xsub.recv(msg, zmq::recv_flags::dontwait);
            if (result_opt.has_value()) {
                std::cout << std::format("Received -> {}\n", std::string(static_cast<const char*>(msg.data()), msg.size()));
            }

            std::unique_lock<std::mutex> lock(mutex);
            std::condition_variable_any().wait_for(lock, stoken, 10ms, [] { return false; });
        }

    } catch (const zmq::error_t& excep) {
        std::cerr << "subscriber exception: " << excep.what() << std::endl;
    }
}

void publisher(std::stop_source& stop_source, zmq::context_t& ctx)
{
    using namespace std::chrono_literals;
    try {
        zmq::socket_t xpub(ctx, zmq::socket_type::pub);
        xpub.connect(ZMQ_XSUB_URL);

        SocketMonitor monitor;
        monitor.init(xpub, "inproc://monitor_" + std::to_string(reinterpret_cast<uint64_t>(&xpub)));

        monitor.watchEvent(SocketMonitor::EventType::Disconnected, [&stop_source] {
            stop_source.request_stop();
        });

        const auto& stoken = stop_source.get_token();
        ElapsedTimer timer;
        std::mutex mutex;
        timer.start();

        while (!stoken.stop_requested()) {
            monitor.check_event();

            if (timer.hasExpired(1000)) {
                xpub.send(zmq::str_buffer(LOCAL_TOPIC), zmq::send_flags::sndmore);
                xpub.send(zmq::buffer("hello"));
                timer.restart();
            }

            std::unique_lock<std::mutex> lock(mutex);
            std::condition_variable_any().wait_for(lock, stoken, 10ms, [] { return false; });
        }

    } catch (const zmq::error_t& excep) {
        std::cerr << "publisher exception: " << excep.what() << std::endl;
    }
}

int main(int argc, char* argv[])
{
    using namespace std::chrono_literals;
    zmq::context_t ctx(1);

    std::stop_source sub_stop_source;
    std::thread sub_thd(subscriber, std::ref(sub_stop_source), std::ref(ctx));

    std::stop_source pub_stop_source;
    std::thread pub_thd(publisher, std::ref(pub_stop_source), std::ref(ctx));

    std::string user_input;

    while (std::getline(std::cin, user_input)) {
        if (user_input == "exit") {
            sub_stop_source.request_stop();
            pub_stop_source.request_stop();
            break;
        }
    }

    sub_thd.join();
    pub_thd.join();

    return 0;
}